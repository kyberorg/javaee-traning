package net.virtalab.stub.listener; /**
 * Description here
 *
 * Created at: 10/8/13
 */

import net.virtalab.stub.ejb.stateful.IStateTestLocal;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;

@WebListener()
public class SessionListener implements HttpSessionListener{


    // Public constructor is required by servlet spec
    public SessionListener() {
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
      /* Session is created. */
    }

    public void sessionDestroyed(HttpSessionEvent se) {
      /* Session is destroyed. */
        HttpSession session = se.getSession();

        Object sfBeanObj = session.getAttribute("sfBean");
        if(sfBeanObj!=null){
            IStateTestLocal sfBean = (IStateTestLocal) sfBeanObj;
            sfBean.myRemove();
        }

    }

}
