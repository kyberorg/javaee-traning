package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateful.IStateTestLocal;
import net.virtalab.stub.ejb.stateless.ILocalStub;
import net.virtalab.stub.ejb.stateless.WorkNoIfaceBean;
import net.virtalab.stub.util.Tags;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
public class EJBTestServlet extends HttpServlet {

    //with @LocalBean
    @EJB
    WorkNoIfaceBean noIfaceBean;

    //without @LocalBean
    @EJB
    ILocalStub stubBean;

    //stateful test bean
    @EJB
    IStateTestLocal stateTestLocal;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>EJB tests</h1>");

        String result = noIfaceBean.info("EJB Test Servlet");
        //calling thu beans chain
        String chainResult =stubBean.infoWN();

        String stateResult = stateTestLocal.info();
        stateTestLocal.myRemove();



        //DISPLAY

        htmlBuilder.append("<h1>Stateless</h1>");

        htmlBuilder.append(Tags.LINE);
        htmlBuilder.append(result);
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append(chainResult);
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append("<h1>Stateful</h1>");
        htmlBuilder.append(Tags.LINE);
        htmlBuilder.append(stateResult);
        htmlBuilder.append(Tags.LINE);


        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
