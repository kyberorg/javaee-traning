package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateless.FacadeLocal;
import net.virtalab.stub.util.Tags;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Facade Servlet working with Local EJBs
 * <p/>
 * Created at: 10/3/13
 */

public class FacadeLocalServlet extends HttpServlet {
        @EJB FacadeLocal f;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<body>");
        htmlBuilder.append("<h1>Facade Local works!</h1>");

        if(f!=null){
            htmlBuilder.append("</hr>");
            htmlBuilder.append(f.infoLocal());
        } else {
            htmlBuilder.append("<span class=\"error\">");
            htmlBuilder.append("Error: Local facadeEJB is null");
            htmlBuilder.append("</span>");
            htmlBuilder.append("</hr>");
        }
        htmlBuilder.append(Tags.LINE);
        htmlBuilder.append(Thread.currentThread().getName());
        htmlBuilder.append(Tags.EOL);

        //sleep 3 seconds (just for session visualization)
        try {
            Thread.currentThread().sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        htmlBuilder.append("</body>");
        htmlBuilder.append("</html>");
        //This disables link to f from proxy (calling method unsetSessionContext)
        //f = null;
        out.print(htmlBuilder.toString());
        out.close();

    }
}
