package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateful.IStateTestLocal;
import net.virtalab.stub.util.Tags;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
public class StateSamplesServlet extends HttpServlet {
    @EJB
    IStateTestLocal sfBean;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>Stateful EJB tests</h1>");

        //getting session
        HttpSession session = request.getSession();
        session.setAttribute("sfBean",sfBean);

        //DISPLAY

        htmlBuilder.append("<h1>Session</h1>");
        htmlBuilder.append(Tags.LINE);
        htmlBuilder.append("Session id=").append(session.getId()).append(Tags.EOL);
        htmlBuilder.append("Session isNew=").append(session.isNew()).append(Tags.EOL);
        htmlBuilder.append("Creation time").append(new Date(session.getCreationTime())).append(Tags.EOL);
        htmlBuilder.append("Last access time").append(new Date(session.getLastAccessedTime())).append(Tags.EOL);

        htmlBuilder.append(Tags.LINE);


        htmlBuilder.append("<h1>Stateful</h1>");
        htmlBuilder.append(Tags.LINE);
        //htmlBuilder.append(stateResult);
        htmlBuilder.append(Tags.LINE);


        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
