package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateless.DBLocal;
import net.virtalab.stub.orm.Department;
import net.virtalab.stub.orm.Employee;
import net.virtalab.stub.orm.Person;
import net.virtalab.stub.orm.Report;
import net.virtalab.stub.util.Tags;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

/**
 * Description here
 * <p/>
 * Created at: 10/9/13
 */
public class DBServlet extends HttpServlet {
    @EJB
    private DBLocal d;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>JPA tests</h1>");

        htmlBuilder.append(Tags.LINE);
//        try {
//            d.execSQL("COMMIT;");
//        } catch (Exception e) {
//            htmlBuilder.append(e);
//        }
        Object[] tables = d.getTables();
        List persons = d.getPersons();
        List dept10 = d.generateReport(10);
        List deps = d.getAllDepartments();
        List depsWithLowSalary = d.getDeptBySalaryLessThen(BigDecimal.valueOf(1000));
        Employee emp7 = d.getEmpById(7);

        d.updateSalary(7,BigDecimal.valueOf(1000));

        //DISPLAY
        htmlBuilder.append(Tags.LINE);

//        for(int i=0;i<tables.length;i++){
//            htmlBuilder.append(tables[i]).append(Tags.EOL);
//        }

        for (Object p: persons){
            Person person = (Person) p;
            htmlBuilder.append(person).append(Tags.EOL);
        }
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append(Tags.LINE);
        for (Object reportR: dept10){
            Report reportRaw = (Report) reportR;
            htmlBuilder.append(reportRaw).append(Tags.EOL);
        }
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append(Tags.LINE);
        for(Object deptObj : deps){
            String deptName = ((Department)deptObj).getDeptName();
            htmlBuilder.append(deptName).append(Tags.EOL);
        }
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append(Tags.LINE);
        for(Object deptObj : depsWithLowSalary){
            String deptName = ((Department)deptObj).getDeptName();
            htmlBuilder.append(deptName).append(Tags.EOL);
        }
        htmlBuilder.append(Tags.LINE);
        htmlBuilder.append(emp7.getEmpName()).append(Tags.EOL);
        htmlBuilder.append(Tags.LINE);

        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
