package net.virtalab.stub.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Description here
 * <p/>
 * Created at: 10/5/13
 */

public class TestServlet extends HttpServlet {
    private  static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private PrintWriter out = null;
    private String lt = "<BR>";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        out = response.getWriter();
        response.setContentType(CONTENT_TYPE);

        String number = request.getParameter("num");

        if (number == null){
             out.write("empty");
        }else {
           // Long.MAX_VALUE
            double p = 0;
            try{
                p = Double.parseDouble(number);
            }catch (Exception e){
                out.write("Exception happens"+lt);
                out.write(lt);
                e.printStackTrace(out);
            }
            double pars1 = p+1;
            out.write(pars1+"");
        }

        out.close();
    }
}
