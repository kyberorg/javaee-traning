package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateless.FacadeBean;
import net.virtalab.stub.ejb.stateless.FacadeRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Facade Client
 * <p/>
 * Created at: 10/3/13
 */

public class FacadeServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>Facade works!</h1>");


        //Accessing EJB
        Context ctx = null;
        FacadeRemote facadeEJB = null;
        try {
            ctx = new InitialContext();
            Object o = ctx.lookup("ejb/kyber/FacadeEJB");
            //trying to cast
             facadeEJB = (FacadeRemote) o;
            if(facadeEJB instanceof FacadeBean){
                htmlBuilder.append("Hooray! Got object of class");
            } else {
                htmlBuilder.append("Okay! facadeEJB is instance of:"+facadeEJB.getClass().getCanonicalName());
                htmlBuilder.append("<br>");
            }
        } catch (NamingException e) {
            htmlBuilder.append("<span class=\"error\">");
            htmlBuilder.append("Error "+e);
            htmlBuilder.append("</span>");
            htmlBuilder.append("</hr>");

        } catch (ClassCastException cce){
            htmlBuilder.append("<span class=\"error\">");
            htmlBuilder.append("Error "+cce);
            htmlBuilder.append("</span>");
            htmlBuilder.append("</hr>");
        }

        if(facadeEJB!=null){
            htmlBuilder.append("</hr>");
            htmlBuilder.append(facadeEJB.info());
        } else {
            htmlBuilder.append("<span class=\"error\">");
            htmlBuilder.append("Error: facadeEJB is null");
            htmlBuilder.append("</span>");
            htmlBuilder.append("</hr>");
        }

        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
