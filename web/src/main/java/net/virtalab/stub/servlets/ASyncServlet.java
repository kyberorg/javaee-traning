package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateless.ITimerLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
public class ASyncServlet extends HttpServlet {
    @EJB
    ITimerLocal t;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>Async test</h1>");

        String asyncResult = t.testASync();

        htmlBuilder.append("<HR>");

        htmlBuilder.append(asyncResult);

        htmlBuilder.append("<HR>");

        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
