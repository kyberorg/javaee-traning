package net.virtalab.stub.servlets;

import net.virtalab.stub.ejb.stateless.SingletonClientLocal;
import net.virtalab.stub.util.Tags;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
public class SingletonDemoServlet extends HttpServlet {
    @EJB
    private SingletonClientLocal singletonClient;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        StringBuilder htmlBuilder = new StringBuilder();
        htmlBuilder.append("<html>");
        //css
        htmlBuilder.append("<head>");
        htmlBuilder.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css\\app.css\">");
        htmlBuilder.append("</head>");
        htmlBuilder.append("<h1>Singleton EJB tests</h1>");

        htmlBuilder.append("<h1>Singleton</h1>");
        htmlBuilder.append(Tags.LINE);

        //calling read method
        for (int i = 0; i < 10; i++) {
            String info = singletonClient.getSingletonInfo();
            htmlBuilder.append(info).append(" " + i).append(Tags.EOL);
        }

        //calling write method
        htmlBuilder.append(Tags.LINE);

        //calling read method
        for (int i = 0; i < 10; i++) {
            String info1 = "InfoWrite";
            singletonClient.setSingletonInfo(info1+" "+i);
            htmlBuilder.append(info1).append(" "+i).append(Tags.EOL);
        }

        //htmlBuilder.append(stateResult);
        htmlBuilder.append(Tags.LINE);


        htmlBuilder.append("</html>");

        out.print(htmlBuilder.toString());
        out.close();
    }
}
