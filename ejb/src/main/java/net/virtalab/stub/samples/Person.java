package net.virtalab.stub.samples;

import java.io.Serializable;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
public class Person implements Serializable {
    private String fn;
    private String sn;
    private String info;

   public Person(){}

    public String getFn() {

        return fn;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


}
