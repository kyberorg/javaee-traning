package net.virtalab.stub.ejb.stateless;

import net.virtalab.stub.samples.Person;

import javax.annotation.PostConstruct;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import java.util.concurrent.Future;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Stateless
public class WorkASyncBean implements WorkASyncLocal {
    public WorkASyncBean() {
    }

    @Override
    @Asynchronous
    public void info1() {
        System.out.println("-----------------------------");
        for (int i = 0; i < 10 ; i++) {
            try {
                Thread.currentThread().sleep(500);
                System.out.println("======="+i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    @Asynchronous
    public Future infoWithFuture(){
        System.out.println("+++++++++++++BEFORE+++++++++++++++++");
        Person p = new Person();
        p.setFn("Jussi");
        p.setSn("Jarvinen");
        p.setInfo("Karhunjarvi");
        AsyncResult ret = new AsyncResult(p);
        System.out.println("+++++++++++++AFTER+++++++++++++++++");
        return ret;
    }

    @PostConstruct
    public void init(){
        System.out.println("-++++++++++++++");
    }
}
