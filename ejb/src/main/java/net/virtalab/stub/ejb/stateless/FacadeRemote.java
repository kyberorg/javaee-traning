package net.virtalab.stub.ejb.stateless;

import javax.ejb.Remote;

/**
 * Description here
 * <p/>
 * Created at: 10/3/13
 */
@Remote
public interface FacadeRemote {
    String info();
}
