package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;

/**
 * Timer Local Interface
 * <p/>
 * Created at: 10/8/13
 */
@Local
public interface ITimerLocal {
     void startJob();
     void stopJob();
     String testASync();
}
