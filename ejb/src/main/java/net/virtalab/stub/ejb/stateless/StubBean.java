package net.virtalab.stub.ejb.stateless;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Easy Bean
 * <p/>
 * Created at: 10/3/13
 */
@Stateless(mappedName = "StubBean")
public class StubBean implements IStub,ILocalStub {
    @EJB
    private WorkNoIfaceBean wn;

    private Long id;
    public StubBean(){
        //Not To do like this
        id=System.currentTimeMillis();
        //System.out.println("--------StubBean id"+id+" created ------------------------");
    }

    @Override
    public String info() {
        return "I am StubBean";
    }

    @Override
    public String infoWN(){
        return wn.info("StubBean");
    }
}
