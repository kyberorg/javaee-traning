package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Local
public interface SingletonClientLocal {
    String getSingletonInfo();
    void setSingletonInfo(String info);
}
