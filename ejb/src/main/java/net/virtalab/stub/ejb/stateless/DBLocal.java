package net.virtalab.stub.ejb.stateless;

import net.virtalab.stub.orm.Department;
import net.virtalab.stub.orm.Employee;
import net.virtalab.stub.orm.Person;
import net.virtalab.stub.orm.Report;

import javax.ejb.Local;
import java.math.BigDecimal;
import java.util.List;

/**
 * Description here
 * <p/>
 * Created at: 10/9/13
 */
@Local
public interface DBLocal {
    String info();
    void execSQL(String sql) throws Exception;
    Object[] getTables();
    List<Person> getPersons();
    List<Report> generateReport(int id);
    List<Department> getAllDepartments();
    List<Department> getDeptBySalaryLessThen(BigDecimal salary);
    Employee getEmpById(Integer id);

    void updateSalary(int employeeId,BigDecimal newSalary);

}
