package net.virtalab.stub.ejb.stateless;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Stateless(name = "WorkNoIfaceEJB")
@LocalBean
public class WorkNoIfaceBean {
    public WorkNoIfaceBean() {
    }

    public String info(String name){
        return "NoIFace called by"+name;
    }
}
