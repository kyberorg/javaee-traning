package net.virtalab.stub.ejb.stateless;

import net.virtalab.stub.samples.Person;

import javax.annotation.Resource;
import javax.ejb.*;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Bean which works with Timer
 * <p/>
 * Created at: 10/8/13
 */
@Stateless
@LocalBean
public class TimerBean implements ITimerLocal {
    @EJB
    private WorkASyncLocal w;

    TimerHandle th = null;
    @Resource
    private SessionContext sCtx;

    private static final String TAG = "TimerTag";
    private static final String TIMER_NAME = "TimerJob";
    public TimerBean(){}

    @Override
    public void startJob() {
        if(th==null){
            //task not working
            //start Date
            //duration
            //desc
            th = sCtx.getTimerService()
                    .createTimer(new Date(), 2000, TIMER_NAME).getHandle();
        }
    }
    @Override
    public void stopJob() {
//        if(th!=null){
//            th.getTimer().cancel();
//            th=null;
//        }
        Collection<Timer> timers = sCtx.getTimerService().getTimers();
        for(Timer t: timers ){
            t.cancel();
        }
    }

    @Timeout
    public void invokeJob(Timer t){
        if(t.getInfo().equals(TIMER_NAME)){
            System.out.println("-------------"+new Date());
        }
    }

    @Override
    public String testASync() {
        //invokeASync():
        w.info1();
        Future rslt = w.infoWithFuture();
        Person p;
        try {
            //p = (Person) rslt.get();
            //set timeout for 2 seconds;
            p = (Person) rslt.get(2, TimeUnit.SECONDS);
            return p.getFn()+" "+p.getSn();
        } catch (InterruptedException e) {
            return "InterruptedException "+e;
        } catch (ExecutionException e) {
            return "ExecutionException "+e;
        } catch (TimeoutException e) {
            return "Time exceeds";
        }

    }
}
