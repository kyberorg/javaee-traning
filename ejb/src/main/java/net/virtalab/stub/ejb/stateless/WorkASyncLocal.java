package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;
import java.util.concurrent.Future;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Local
public interface WorkASyncLocal {
    void info1();
    Future  infoWithFuture();
}
