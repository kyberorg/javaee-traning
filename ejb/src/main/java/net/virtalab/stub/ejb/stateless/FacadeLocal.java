package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;

/**
 * Local interface for Facade EJB
 * <p/>
 * Created at: 10/3/13
 */
@Local
public interface FacadeLocal {
    public String infoLocal();
}
