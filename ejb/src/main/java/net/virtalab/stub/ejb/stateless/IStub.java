package net.virtalab.stub.ejb.stateless;

import javax.ejb.Remote;

/**
 * Bean interface
 * <p/>
 * Created at: 10/3/13
 */
@Remote
public interface IStub {
    String info();
}
