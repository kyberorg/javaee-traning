package net.virtalab.stub.ejb.stateful;

import javax.ejb.Local;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Local
public interface IStateTestLocal {
    void myRemove();
    String info();
}
