package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;

/**
 * Local interface
 * <p/>
 * Created at: 10/3/13
 */
@Local
public interface ILocalStub {
    String info();
    String infoWN();
}
