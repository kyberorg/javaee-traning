package net.virtalab.stub.ejb.singleton;

import javax.ejb.Local;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Local
public interface MySingleLocal {
    String getMyInfo();
    void setMyInfo(String info);
}
