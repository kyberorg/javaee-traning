package net.virtalab.stub.ejb.stateless;

import javax.ejb.Local;

/**
 * Description here
 * <p/>
 * Created at: 10/14/13
 */
@Local
public interface WSLocal {
    int info(String t);
}
