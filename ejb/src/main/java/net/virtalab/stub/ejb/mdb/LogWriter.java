package net.virtalab.stub.ejb.mdb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Description here
 * <p/>
 * Created at: 10/14/13
 */
@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(propertyName  = "destinationType", propertyValue = "javax.jms.Queue"),

                @ActivationConfigProperty(propertyName  = "connectionFactoryJndiName", propertyValue = "java:/ConnectionFactory"), // External JNDI Name

                @ActivationConfigProperty(propertyName  = "destination", propertyValue = "java:/queue/kyber") // Ext. JNDI Name
        }
)
public class LogWriter implements MessageListener {
    public LogWriter() {
    }

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        String logString = null;
        try {
            logString = textMessage.getText();
        } catch (JMSException e) {
            System.out.println(e);
        }

        System.out.println("=============Got message:"+logString+"==================");
    }
}
