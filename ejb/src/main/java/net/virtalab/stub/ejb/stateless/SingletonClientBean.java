package net.virtalab.stub.ejb.stateless;

import net.virtalab.stub.ejb.singleton.MySingleLocal;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Stateless
public class SingletonClientBean implements SingletonClientLocal {
    @EJB
    private MySingleLocal s;
    public SingletonClientBean() {
    }

    @Override
    public String getSingletonInfo() {
        return s.getMyInfo();
    }

    @Override
    public void setSingletonInfo(String str) {
        s.setMyInfo(str);
    }
}
