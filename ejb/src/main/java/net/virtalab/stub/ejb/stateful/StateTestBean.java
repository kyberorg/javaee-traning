package net.virtalab.stub.ejb.stateful;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Stateful
public class StateTestBean implements IStateTestLocal {
    private static final String TAG = "StateTestBean";
    private long id =0;
    public StateTestBean() {
    }

    @Override
    @Remove
    public void myRemove() {
        System.out.println("-------REMOVE "+TAG+" (ID: "+id+")");
    }

    @Override
    public String info() {
        return "Terve mina olen "+id;
    }

    @PostConstruct
    public void init(){
        id = System.currentTimeMillis();
        System.out.println("-------INIT "+TAG+" (ID: "+id+")");
    }

    @PostActivate
    public void activate(){
        System.out.println("-------Activate "+TAG+" (ID: "+id+")");
    }

    @PrePassivate
    public void passivate(){
        System.out.println("-------Passivate "+TAG+" (ID: "+id+")");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("-------Destroy "+TAG+" (ID: "+id+")");
    }
}
