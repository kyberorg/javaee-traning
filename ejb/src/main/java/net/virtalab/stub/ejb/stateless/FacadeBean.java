package net.virtalab.stub.ejb.stateless;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 * Description here
 * <p/>
 * Created at: 10/3/13
 */
@Stateless
@LocalBean
@Local(FacadeLocal.class)
public class FacadeBean implements FacadeLocal, FacadeRemote {
    private static final String TAG = "FacadeTag";
    private long id;

    @Resource(mappedName = "java:/jdbc/kyberdb")
    private DataSource ds;

    @Resource
    private SessionContext sCtx;

    public FacadeBean() {
        //TODO Remove before production
        id=System.currentTimeMillis();
        System.out.println(TAG+" Constructor Bean ID="+id);
        System.out.println(TAG+" Data Source "+ds);
    }

    @PostConstruct
    public void myInit(){
        System.out.println(TAG+" PostConstructor Bean ID="+id);
        System.out.println(TAG+" Data Source "+ds);
    }

    @Override
    public String info() {
        System.out.println(TAG+" Method info() id="+id);
        return "My id is "+id;
    }

    @Override
    public String infoLocal() {
        StringBuilder reply = new StringBuilder();
        reply.append("I am local. My id is ").append(id);
         reply.append("<BR> Session Context:").append(sCtx);
        return reply.toString();
    }

    @PreDestroy
    public void myDestroy(){
        System.out.println(TAG+" PreDestroy Bean ID="+id);
    }
}
