package net.virtalab.stub.ejb.stateless;

import javax.ejb.Stateless;
import javax.jws.WebService;

/**
 * Description here
 * <p/>
 * Created at: 10/14/13
 */
@Stateless
@WebService(name = "WS", serviceName = "WS", portName = "WSPort")
public class WSBean implements WSLocal {
    public WSBean() {
    }

    @Override
    public int info(String t) {
        System.out.println("-------------- info() at WS");
        return t.length();
    }
}
