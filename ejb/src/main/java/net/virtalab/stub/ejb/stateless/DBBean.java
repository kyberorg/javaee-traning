package net.virtalab.stub.ejb.stateless;

import net.virtalab.stub.orm.*;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Description here
 * <p/>
 * Created at: 10/9/13
 */
@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER) //who makes Bean Transactions
public class DBBean implements DBLocal {
    @PersistenceContext(unitName = "kyberdb")
    private EntityManager em;
    @PersistenceContext(unitName = "kyberlog")
    private EntityManager logEm;

    @Resource
    private UserTransaction tx;

    @Resource(mappedName = "java:/ConnectionFactory")
    ConnectionFactory factory;

    @Resource(mappedName = "java:/queue/kyber")
    Queue q;

    public DBBean() {
    }

    @Override
    public String info() {
        String str ="";
        try {
            tx.begin();
            str = em.toString();
            tx.commit();
        } catch (Exception e) {
            try {
                tx.rollback();
            } catch (SystemException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();

        }

        return str;
    }

    @Override
    public void execSQL(String sql) throws Exception {
        int c = em.createNativeQuery(sql).executeUpdate();
        System.out.println(c+" rows affected");

    }

    @Override
    public Object[] getTables() {
        return em.createNativeQuery("SHOW TABLES").getResultList().toArray();
    }

    @Override
    public List<Person> getPersons() {
        return em.createNativeQuery("SELECT emp_name,emp_job,emp_salary,emp_id FROM employee",Person.class).getResultList();
    }

    @Override
    public List<Report> generateReport(int deptId) {
        String sql = "select dept_name,emp_name,emp_job,emp_salary, emp_comm " +
                     "from department, employee " +
                     "where employee.emp_dept = department.dept_id and employee.emp_dept= ?;";
        List result = em.createNativeQuery(sql, Report.class).setParameter(1,Long.valueOf(deptId)).getResultList();
        return result;
    }

    @Override
    public List<Department> getAllDepartments() {
        return em.createNamedQuery("dept.GetAll").getResultList();
    }

    @Override
    public List<Department> getDeptBySalaryLessThen(BigDecimal salary) {
        return em.createNamedQuery("dept.GetBySalary").setParameter("p", salary).getResultList();
    }

    @Override
    public Employee getEmpById(Integer id) {
        return em.find(Employee.class, id);
    }

    @Override
    public void updateSalary(int employeeId, BigDecimal newSalary) {
        Employee e = getEmpById(employeeId);
            BigDecimal oldSalary = e.getEmpSalary();
            e.setEmpSalary(newSalary);
            //do update
            em.merge(e);
//            //logging this action
//            LogTable logTable = new LogTable();
            StringBuilder text = new StringBuilder();
            text.append("Updated salary for ");
            text.append("employee Id ").append(employeeId);
            text.append(" Old Salary is: ").append(oldSalary);
            text.append(" New Salary is: ").append(newSalary);
//
//            //logTable.setLogId(null);
//            logTable.setLogText(text.toString());
//
//            //insert
//            logEm.persist(logTable);
         Connection connection = null;
         Session session = null;
         MessageProducer messageProducer = null;
         try{
             connection = factory.createConnection();
             //true - proccess is part of transaction
             session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
             //producer - creator of message
             messageProducer = session.createProducer(q);

             TextMessage message = session.createTextMessage();
             message.setText("Message for JMS "+text);

             messageProducer.send(message);

         } catch (Exception e2){
             System.out.println("Exception "+e2);
         } finally {
             if (connection != null) {
                try {
                     connection.close();
                } catch (JMSException e1) {
                      e1.printStackTrace();
                }
             }
             if(session != null){
                 try {
                     session.close();
                 } catch (JMSException e1) {
                     e1.printStackTrace();
                 }
             }
             if(messageProducer != null){
                 try {
                     messageProducer.close();
                 } catch (JMSException e1) {
                     e1.printStackTrace();
                 }
             }
         }
    }
}
