package net.virtalab.stub.ejb.singleton;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * Description here
 * <p/>
 * Created at: 10/8/13
 */
@Singleton
@Startup
public class MySingleBean implements MySingleLocal {
    private String myInfo = "MySingleBean";

    public MySingleBean() {
    }

    @PostConstruct
    public void init(){
        System.out.println("====Singleton created====");
    }

    @Override
    @Lock(value = LockType.READ)
    public String getMyInfo(){
        System.out.println("---getMyInfo() working at "+Thread.currentThread().getId()+"---");
        //hold
        for (int i = 0; i < 5; i++) {
            try{
                Thread.currentThread().sleep(500);
            }catch (Exception e){
                //stub
            }

        }
        return this.myInfo;
    }

    @Override
    @Lock(value = LockType.WRITE)
    public void setMyInfo(String str) {
        myInfo = str;

        //hold
        for (int i = 0; i < 5; i++) {
            try{
                Thread.currentThread().sleep(500);
            }catch (Exception e){
                //stub
            }
        }
        System.out.println("---setMyInfo() working at "+Thread.currentThread().getId()+"---");
    }
}
