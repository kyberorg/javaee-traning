package net.virtalab.stub.orm;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Description here
 * <p/>
 * Created at: 10/9/13
 */
@Entity
@Table(name="employee")
public class Report implements Serializable{
    @Column(name = "dept_name")
    private String dname;
    @Id
    @Column(name="emp_name")
    private String ename;
    @Column(name="emp_job")
    private String job;
    @Column(name="emp_salary")
    private Long salary;
    @Column(name="emp_comm")
    private Long comm;
    @Transient
    private Long allSalary;

    public Long getAllSalary() {
        return allSalary;
    }

    public void setAllSalary(Long allSalary) {
        this.allSalary = allSalary;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Long getComm() {
        return comm;
    }

    public void setComm(Long comm) {
        this.comm = comm;
    }

    @PostLoad
    public void checkAllSal(){
        allSalary = getSalary()*12;
        if(getComm()!=null){
           allSalary = allSalary + getComm();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName()).append(" [");
        sb.append(" ").append("dname ").append(dname);
        sb.append(" ").append("ename ").append(ename);
        sb.append(" ").append("job ").append(job);
        sb.append(" ").append("salary ").append(salary);
        sb.append(" ").append("Comm ").append(comm);
        sb.append(" ").append("year salary ").append(allSalary);
        sb.append("]");

        return sb.toString();
    }

}
