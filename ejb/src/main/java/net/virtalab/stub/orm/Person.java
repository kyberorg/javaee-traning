package net.virtalab.stub.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Description here
 * <p/>
 * Created at: 10/9/13
 */
@Entity(name = "Person")
@Table(name = "employee")

public class Person implements Serializable {
    @Column(name="emp_name")
    private String name;
    @Column(name="emp_job")
    private String job;
    @Id
    @Column(name="emp_id")
    private Long id;
    @Column(name = "emp_salary")
    private BigInteger salary;


    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getSalary() {
        return salary;
    }

    public void setSalary(BigInteger salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
         sb.append(this.getClass().getSimpleName()).append(" [");
         sb.append(" ").append("name ").append(name);
         sb.append(" ").append("job ").append(job);
         sb.append(" ").append("id ").append(id);
         sb.append(" ").append("salary ").append(salary);
         sb.append("]");

        return sb.toString();
    }
}
