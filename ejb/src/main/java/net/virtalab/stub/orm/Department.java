package net.virtalab.stub.orm;

import javax.persistence.*;
import java.util.Collection;

/**
 * Description here
 * <p/>
 * Created at: 10/12/13
 */
@Entity(name ="Departments")
@Table(name="department")
@NamedQueries(value = {
        @NamedQuery(name="dept.GetAll",query = "select o from Departments o"),
        @NamedQuery(name="dept.GetBySalary",query = "select distinct o from Departments o, IN(o.employeesByDeptId) a where a.empSalary <= :p")
})

public class Department {
    private int deptId;
    private String deptName;
    private String deptLocation;
    private Collection<Employee> employeesByDeptId;

    @javax.persistence.Column(name = "dept_id")
    @Id
    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    @javax.persistence.Column(name = "dept_name")
    @Basic
    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @javax.persistence.Column(name = "dept_location")
    @Basic
    public String getDeptLocation() {
        return deptLocation;
    }

    public void setDeptLocation(String deptLocation) {
        this.deptLocation = deptLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (deptId != that.deptId) return false;
        if (deptLocation != null ? !deptLocation.equals(that.deptLocation) : that.deptLocation != null) return false;
        if (deptName != null ? !deptName.equals(that.deptName) : that.deptName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = deptId;
        result = 31 * result + (deptName != null ? deptName.hashCode() : 0);
        result = 31 * result + (deptLocation != null ? deptLocation.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "departmentByEmpDept")
    public Collection<Employee> getEmployeesByDeptId() {
        return employeesByDeptId;
    }

    public void setEmployeesByDeptId(Collection<Employee> employeesByDeptId) {
        this.employeesByDeptId = employeesByDeptId;
    }
}
