package net.virtalab.stub.orm;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Collection;

/**
 * Description here
 * <p/>
 * Created at: 10/12/13
 */
@Entity
public class Employee {
    private int empId;
    private String empName;
    private String empJob;
    private Date empHiredate;
    private BigDecimal empSalary;
    private BigDecimal empComm;
    private Employee employeeByEmpBoss;
    private Collection<Employee> employeesByEmpId;
    private Department departmentByEmpDept;

    @javax.persistence.Column(name = "emp_id")
    @Id
    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @javax.persistence.Column(name = "emp_name")
    @Basic
    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @javax.persistence.Column(name = "emp_job")
    @Basic
    public String getEmpJob() {
        return empJob;
    }

    public void setEmpJob(String empJob) {
        this.empJob = empJob;
    }

    @javax.persistence.Column(name = "emp_hiredate")
    @Basic
    public Date getEmpHiredate() {
        return empHiredate;
    }

    public void setEmpHiredate(Date empHiredate) {
        this.empHiredate = empHiredate;
    }

    @javax.persistence.Column(name = "emp_salary")
    @Basic
    public BigDecimal getEmpSalary() {
        return empSalary;
    }

    public void setEmpSalary(BigDecimal empSalary) {
        this.empSalary = empSalary;
    }

    @javax.persistence.Column(name = "emp_comm")
    @Basic
    public BigDecimal getEmpComm() {
        return empComm;
    }

    public void setEmpComm(BigDecimal empComm) {
        this.empComm = empComm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (empId != employee.empId) return false;
        if (empComm != null ? !empComm.equals(employee.empComm) : employee.empComm != null) return false;
        if (empHiredate != null ? !empHiredate.equals(employee.empHiredate) : employee.empHiredate != null)
            return false;
        if (empJob != null ? !empJob.equals(employee.empJob) : employee.empJob != null) return false;
        if (empName != null ? !empName.equals(employee.empName) : employee.empName != null) return false;
        if (empSalary != null ? !empSalary.equals(employee.empSalary) : employee.empSalary != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = empId;
        result = 31 * result + (empName != null ? empName.hashCode() : 0);
        result = 31 * result + (empJob != null ? empJob.hashCode() : 0);
        result = 31 * result + (empHiredate != null ? empHiredate.hashCode() : 0);
        result = 31 * result + (empSalary != null ? empSalary.hashCode() : 0);
        result = 31 * result + (empComm != null ? empComm.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @javax.persistence.JoinColumn(name = "emp_boss", referencedColumnName = "emp_id", nullable = false)
    public Employee getEmployeeByEmpBoss() {
        return employeeByEmpBoss;
    }

    public void setEmployeeByEmpBoss(Employee employeeByEmpBoss) {
        this.employeeByEmpBoss = employeeByEmpBoss;
    }

    @OneToMany(mappedBy = "employeeByEmpBoss")
    public Collection<Employee> getEmployeesByEmpId() {
        return employeesByEmpId;
    }

    public void setEmployeesByEmpId(Collection<Employee> employeesByEmpId) {
        this.employeesByEmpId = employeesByEmpId;
    }

    @ManyToOne
    @JoinColumn(name = "emp_dept", referencedColumnName = "dept_id")
    public Department getDepartmentByEmpDept() {
        return departmentByEmpDept;
    }

    public void setDepartmentByEmpDept(Department departmentByEmpDept) {
        this.departmentByEmpDept = departmentByEmpDept;
    }
}
