package net.virtalab.stub.orm;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Description here
 * <p/>
 * Created at: 10/13/13
 */
@Entity
@Table(name = "log")
public class LogTable implements Serializable {
    @Id
    @GenericGenerator(name="logTableGen",strategy = "increment")
    @GeneratedValue(generator = "logTableGen")
    @Column(name = "log_id")
    private Integer logId;
    @Column(name= "log_text")
    private String logText;
    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public String getLogText() {
        return logText;
    }

    public void setLogText(String logText) {
        this.logText = logText;
    }

}
