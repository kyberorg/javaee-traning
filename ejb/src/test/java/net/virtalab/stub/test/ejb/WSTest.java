package net.virtalab.stub.test.ejb;

import net.virtalab.stub.ejb.webservices.WS;
import net.virtalab.stub.ejb.webservices.WS_Service;

/**
 * Test for WebService
 * <p/>
 * Created at: 10/14/13
 */
public class WSTest {
    //TODO modify to unit test
    public static void main(String[] args){
        WS_Service ws_service = new WS_Service();
        WS ws = ws_service.getWSPort();

        //can call bussiness methods
        System.out.println(ws.info("Str"));
    }
}
