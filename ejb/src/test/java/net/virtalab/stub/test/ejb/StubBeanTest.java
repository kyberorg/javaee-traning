package net.virtalab.stub.test.ejb;

import junit.framework.Assert;
import net.virtalab.stub.ejb.stateless.StubBean;
import org.junit.Test;

/**
 * StubBean Test
 * <p/>
 * Created at: 10/3/13
 */
public class StubBeanTest {

    @Test
    public void stubBeanTest(){
        //adjust
        StubBean bean = new StubBean();
        String validResult="I am StubBean";
        //act
        String actualResult = bean.info();
        //assert
        Assert.assertEquals(validResult,actualResult);
    }
}
