use kyberdb;

select dname,ename,job, sal, (sal*12)+ifnull(comm,0) as year_salary from dept, emp where emp.deptno = dept.deptno;