CREATE USER 'kyberorg'@'127.0.0.1' IDENTIFIED BY 'kyberpassa';
CREATE USER 'logger'@'127.0.0.1' IDENTIFIED BY 'log';
GRANT ALL ON kyberdb.department TO 'kyberorg'@'127.0.0.1';
GRANT ALL ON kyberdb.employee TO 'kyberorg'@'127.0.0.1';
GRANT ALL ON kyberdb.log TO 'logger'@'127.0.0.1';