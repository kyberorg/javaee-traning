CREATE DATABASE  IF NOT EXISTS `kyberdb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `kyberdb`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: kyberdb
-- ------------------------------------------------------
-- Server version	5.6.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `emp_job` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `emp_boss` int(11) NOT NULL,
  `emp_hiredate` date NOT NULL,
  `emp_salary` decimal(9,2) NOT NULL,
  `emp_comm` decimal(9,2) DEFAULT NULL,
  `emp_dept` int(11) DEFAULT NULL,
  PRIMARY KEY (`emp_id`),
  KEY `fk_emp_dept_idx` (`emp_dept`),
  KEY `fk_emp_boss_idx` (`emp_boss`),
  CONSTRAINT `fk_emp_boss` FOREIGN KEY (`emp_boss`) REFERENCES `employee` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_emp_dept` FOREIGN KEY (`emp_dept`) REFERENCES `department` (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT  IGNORE INTO `employee` (`emp_id`, `emp_name`, `emp_job`, `emp_boss`, `emp_hiredate`, `emp_salary`, `emp_comm`, `emp_dept`) VALUES (1,'KING','PRESIDENT',1,'1981-11-17',5000.00,NULL,10),(2,'JONES','MANAGER',1,'1981-04-02',2975.00,NULL,20),(3,'BLAKE','MANAGER',1,'1981-05-01',2850.00,NULL,30),(4,'CLARK','MANAGER',1,'1981-06-09',2450.00,NULL,10),(5,'FORD','ANALYST',2,'1981-12-03',3000.00,NULL,20),(6,'SMITH','CLERK',5,'1980-12-17',800.00,NULL,20),(7,'ALLEN','SALESMAN',3,'1981-02-20',1600.00,300.00,30),(8,'WARD','SALESMAN',3,'1981-02-22',1250.00,500.00,30),(9,'MARTIN','SALESMAN',3,'1981-04-02',2975.00,NULL,20),(10,'SCOTT','ANALYST',2,'1982-12-09',3000.00,NULL,20),(11,'TURNER','SALESMAN',3,'1981-09-08',1500.00,0.00,30),(12,'ADAMS','CLERK',10,'1983-01-12',1100.00,NULL,20),(13,'JAMES','CLERK',3,'1981-12-03',950.00,NULL,30),(14,'MILLER','CLERK',4,'1982-01-23',1300.00,NULL,10);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-12 14:17:45
